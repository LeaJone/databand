package org.databandtech.job.sink;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import org.databandtech.job.entity.SaveMeta;
import org.databandtech.job.entity.tuple.Tuple3;

/**
 * 每种sink导出数据都要写独立泛型类型的sink模板类
 *
 */
public class HiveSqlQueryJob1MySQLSink implements SinkFunction<Tuple3<Integer,String,String>> {

	private static final long serialVersionUID = 1L;
	Connection connection = null;
    PreparedStatement preparedStatement = null;
    SaveMeta saveMeta;
    public HiveSqlQueryJob1MySQLSink(SaveMeta saveMeta) {
		super();
		this.saveMeta = saveMeta;
	}

    @Override
    public void open() throws Exception {
        Class.forName( "com.mysql.jdbc.Driver" );
        connection = DriverManager.getConnection( saveMeta.getUrl(), saveMeta.getUsername(), saveMeta.getPassword() );
        preparedStatement = connection.prepareStatement(saveMeta.getSql());
    }

    @Override
    public void close() throws Exception {
        if (connection != null) {
            connection.close();
        }
        if (preparedStatement != null) {
            preparedStatement.close();
        }
    }

    @Override
    public void invoke(Tuple3<Integer,String,String> tuple) throws Exception {
        try {
        	if (tuple._1()!=null)
            preparedStatement.setInt( 1, tuple._1() );
        	if (tuple._1()!=null)
            preparedStatement.setString( 2, tuple._2() );
        	if (tuple._1()!=null)
            preparedStatement.setString( 3, tuple._3() );
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
