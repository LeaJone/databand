package com.ruoyi.web.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.web.mapper.DatabandSourceMapper;
import com.ruoyi.web.domain.DatabandSource;
import com.ruoyi.web.service.IDatabandSourceService;
import com.ruoyi.common.core.text.Convert;

/**
 * 数据源Service业务层处理
 * 
 * @author databand
 * @date 2020-12-31
 */
@Service
public class DatabandSourceServiceImpl implements IDatabandSourceService 
{
    @Autowired
    private DatabandSourceMapper databandSourceMapper;

    /**
     * 查询数据源
     * 
     * @param id 数据源ID
     * @return 数据源
     */
    @Override
    public DatabandSource selectDatabandSourceById(Long id)
    {
        return databandSourceMapper.selectDatabandSourceById(id);
    }

    /**
     * 查询数据源列表
     * 
     * @param databandSource 数据源
     * @return 数据源
     */
    @Override
    public List<DatabandSource> selectDatabandSourceList(DatabandSource databandSource)
    {
        return databandSourceMapper.selectDatabandSourceList(databandSource);
    }

    /**
     * 新增数据源
     * 
     * @param databandSource 数据源
     * @return 结果
     */
    @Override
    public int insertDatabandSource(DatabandSource databandSource)
    {
        return databandSourceMapper.insertDatabandSource(databandSource);
    }

    /**
     * 修改数据源
     * 
     * @param databandSource 数据源
     * @return 结果
     */
    @Override
    public int updateDatabandSource(DatabandSource databandSource)
    {
        return databandSourceMapper.updateDatabandSource(databandSource);
    }

    /**
     * 删除数据源对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteDatabandSourceByIds(String ids)
    {
        return databandSourceMapper.deleteDatabandSourceByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除数据源信息
     * 
     * @param id 数据源ID
     * @return 结果
     */
    @Override
    public int deleteDatabandSourceById(Long id)
    {
        return databandSourceMapper.deleteDatabandSourceById(id);
    }

	@Override
	public List<DatabandSource> selectDatabandSourceList() {
		return databandSourceMapper.selectDatabandSourceListAll();
	}
}
