package com.ruoyi.web.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.web.mapper.DatabandReporttabMapper;
import com.ruoyi.web.domain.DatabandReporttab;
import com.ruoyi.web.service.IDatabandReporttabService;
import com.ruoyi.common.core.text.Convert;

/**
 * 报页Service业务层处理
 * 
 * @author databand
 * @date 2020-12-31
 */
@Service
public class DatabandReporttabServiceImpl implements IDatabandReporttabService 
{
    @Autowired
    private DatabandReporttabMapper databandReporttabMapper;

    /**
     * 查询报页
     * 
     * @param id 报页ID
     * @return 报页
     */
    @Override
    public DatabandReporttab selectDatabandReporttabById(Long id)
    {
        return databandReporttabMapper.selectDatabandReporttabById(id);
    }

    /**
     * 查询报页列表
     * 
     * @param databandReporttab 报页
     * @return 报页
     */
    @Override
    public List<DatabandReporttab> selectDatabandReporttabList(DatabandReporttab databandReporttab)
    {
        return databandReporttabMapper.selectDatabandReporttabList(databandReporttab);
    }

    /**
     * 新增报页
     * 
     * @param databandReporttab 报页
     * @return 结果
     */
    @Override
    public int insertDatabandReporttab(DatabandReporttab databandReporttab)
    {
        return databandReporttabMapper.insertDatabandReporttab(databandReporttab);
    }

    /**
     * 修改报页
     * 
     * @param databandReporttab 报页
     * @return 结果
     */
    @Override
    public int updateDatabandReporttab(DatabandReporttab databandReporttab)
    {
        return databandReporttabMapper.updateDatabandReporttab(databandReporttab);
    }

    /**
     * 删除报页对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteDatabandReporttabByIds(String ids)
    {
        return databandReporttabMapper.deleteDatabandReporttabByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除报页信息
     * 
     * @param id 报页ID
     * @return 结果
     */
    @Override
    public int deleteDatabandReporttabById(Long id)
    {
        return databandReporttabMapper.deleteDatabandReporttabById(id);
    }
}
