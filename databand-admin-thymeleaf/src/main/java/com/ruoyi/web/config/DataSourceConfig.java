package com.ruoyi.web.config;


import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataSourceConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataSourceConfig.class);
    
    @Bean(name = "dataSourceDrives")
    public Map<String, String> dataSourceDrives() {
    	
    	Map<String, String> map = new HashMap<String, String>();
    	map.put("MySQL5","com.mysql.jdbc.Driver");
    	map.put("ClickHouse","ru.yandex.clickhouse.ClickHouseDriver");
    	map.put("ES","org.elasticsearch.xpack.sql.jdbc.EsDriver");
    	
    	return map;
    }
    
    @Bean(name = "dataSourceURLTemps")
    public Map<String, String> dataSourceURLTemps() {
    	
    	Map<String, String> map = new HashMap<String, String>();
    	map.put("MySQL5","jdbc:mysql://localhost:3306/databand?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8");
    	map.put("ClickHouse","jdbc:clickhouse://localhost:8123/database");
    	map.put("ES","jdbc:es://localhost:9200/");
    	
    	return map;
    }
    
}
