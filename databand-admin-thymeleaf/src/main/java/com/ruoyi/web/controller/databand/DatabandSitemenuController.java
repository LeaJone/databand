package com.ruoyi.web.controller.databand;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.web.domain.DatabandSitemenu;
import com.ruoyi.web.service.IDatabandReportService;
import com.ruoyi.web.service.IDatabandSiteService;
import com.ruoyi.web.service.IDatabandSitemenuService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 站点菜单Controller
 * 
 * @author databand
 * @date 2020-12-31
 */
@Controller
@RequestMapping("/web/sitemenu")
public class DatabandSitemenuController extends BaseController
{
    protected String prefix = "web/sitemenu";

    @Autowired
    private IDatabandSitemenuService databandSitemenuService;
    @Autowired
    private IDatabandSiteService databandSiteService;
    @Autowired
    private IDatabandReportService databandReportService;

    @RequiresPermissions("web:sitemenu:view")
    @GetMapping()
    public String sitemenu()
    {
        return prefix + "/sitemenu";
    }
    
    @RequiresPermissions("web:sitemenu:view")
    @GetMapping("/bind/{id}")
    public String bind(@PathVariable("id") String id,ModelMap mmap)
    {     
        mmap.put("siteid", id);
        String sitename = databandSiteService.selectDatabandSiteById(Long.valueOf(id)).getTitle();
        mmap.put("sitename", sitename);
        return prefix + "/sitemenu";
    }

    /**
     * 查询站点菜单列表
     */
    @RequiresPermissions("web:sitemenu:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(DatabandSitemenu databandSitemenu)
    {
        startPage();
        List<DatabandSitemenu> list = databandSitemenuService.selectDatabandSitemenuList(databandSitemenu);
        return getDataTable(list);
    }

    /**
     * 导出站点菜单列表
     */
    @RequiresPermissions("web:sitemenu:export")
    @Log(title = "站点菜单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(DatabandSitemenu databandSitemenu)
    {
        List<DatabandSitemenu> list = databandSitemenuService.selectDatabandSitemenuList(databandSitemenu);
        ExcelUtil<DatabandSitemenu> util = new ExcelUtil<DatabandSitemenu>(DatabandSitemenu.class);
        return util.exportExcel(list, "sitemenu");
    }

    /**
     * 新增站点菜单
     */
    @GetMapping("/add/{siteid}")
    public String add(@PathVariable("siteid") String siteid, ModelMap mmap)
    {
    	mmap.put("siteid", siteid);
    	mmap.put("report", databandReportService.selectDatabandReportList());
        return prefix + "/add";
    }

    /**
     * 新增保存站点菜单
     */
    @RequiresPermissions("web:sitemenu:add")
    @Log(title = "站点菜单", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(DatabandSitemenu databandSitemenu)
    {
        return toAjax(databandSitemenuService.insertDatabandSitemenu(databandSitemenu));
    }

    /**
     * 修改站点菜单
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        DatabandSitemenu databandSitemenu = databandSitemenuService.selectDatabandSitemenuById(id);
        mmap.put("databandSitemenu", databandSitemenu);
    	mmap.put("report", databandReportService.selectDatabandReportList());

        return prefix + "/edit";
    }

    /**
     * 修改保存站点菜单
     */
    @RequiresPermissions("web:sitemenu:edit")
    @Log(title = "站点菜单", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(DatabandSitemenu databandSitemenu)
    {
        return toAjax(databandSitemenuService.updateDatabandSitemenu(databandSitemenu));
    }

    /**
     * 删除站点菜单
     */
    @RequiresPermissions("web:sitemenu:remove")
    @Log(title = "站点菜单", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(databandSitemenuService.deleteDatabandSitemenuByIds(ids));
    }
}
