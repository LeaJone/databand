package com.ruoyi.web.controller.databand;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.web.domain.DatabandSite;
import com.ruoyi.web.service.IDatabandSiteService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.service.ISysDeptService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 站点Controller
 * 
 * @author databand
 * @date 2020-12-31
 */
@Controller
@RequestMapping("/web/site")
public class DatabandSiteController extends BaseController
{
    private String prefix = "web/site";

    @Autowired
    private IDatabandSiteService databandSiteService;
    
    @Autowired
    private ISysDeptService deptService;

    @RequiresPermissions("web:site:view")
    @GetMapping()
    public String site()
    {
        return prefix + "/site";
    }

    /**
     * 查询站点列表
     */
    @RequiresPermissions("web:site:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(DatabandSite databandSite)
    {
        startPage();
        List<DatabandSite> list = databandSiteService.selectDatabandSiteList(databandSite);
        return getDataTable(list);
    }

    /**
     * 导出站点列表
     */
    @RequiresPermissions("web:site:export")
    @Log(title = "站点", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(DatabandSite databandSite)
    {
        List<DatabandSite> list = databandSiteService.selectDatabandSiteList(databandSite);
        ExcelUtil<DatabandSite> util = new ExcelUtil<DatabandSite>(DatabandSite.class);
        return util.exportExcel(list, "site");
    }

    /**
     * 新增站点
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存站点
     */
    @RequiresPermissions("web:site:add")
    @Log(title = "站点", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(DatabandSite databandSite)
    {
        return toAjax(databandSiteService.insertDatabandSite(databandSite));
    }

    /**
     * 修改站点
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
    	String deptname = "未配置";
        DatabandSite databandSite = databandSiteService.selectDatabandSiteById(id);
        if (databandSite.getDeptid()!=null) {
        	deptname = deptService.selectDeptById(databandSite.getDeptid()).getDeptName();
        }
        
        mmap.put("deptname", deptname);
        mmap.put("databandSite", databandSite);
        return prefix + "/edit";
    }

    /**
     * 修改保存站点
     */
    @RequiresPermissions("web:site:edit")
    @Log(title = "站点", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(DatabandSite databandSite)
    {
        return toAjax(databandSiteService.updateDatabandSite(databandSite));
    }

    /**
     * 删除站点
     */
    @RequiresPermissions("web:site:remove")
    @Log(title = "站点", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(databandSiteService.deleteDatabandSiteByIds(ids));
    }
}
