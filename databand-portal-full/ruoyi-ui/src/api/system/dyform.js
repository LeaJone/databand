import request from '@/utils/request'

// 查询动态单列表
export function listDyform(query) {
  return request({
    url: '/system/dyform/list',
    method: 'get',
    params: query
  })
}

// 查询动态单详细
export function getDyform(id) {
  return request({
    url: '/system/dyform/' + id,
    method: 'get'
  })
}

// 新增动态单
export function addDyform(data) {
  return request({
    url: '/system/dyform',
    method: 'post',
    data: data
  })
}

// 修改动态单
export function updateDyform(data) {
  return request({
    url: '/system/dyform',
    method: 'put',
    data: data
  })
}

// 删除动态单
export function delDyform(id) {
  return request({
    url: '/system/dyform/' + id,
    method: 'delete'
  })
}

// 导出动态单
export function exportDyform(query) {
  return request({
    url: '/system/dyform/export',
    method: 'get',
    params: query
  })
}