-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('动态单', '3', '1', 'dyform', 'system/dyform/index', 1, 0, 'C', '0', '0', 'system:dyform:list', '#', 'admin', sysdate(), '', null, '动态单菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('动态单查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'system:dyform:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('动态单新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'system:dyform:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('动态单修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'system:dyform:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('动态单删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'system:dyform:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('动态单导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'system:dyform:export',       '#', 'admin', sysdate(), '', null, '');