CREATE TABLE default.SNM_initial20210916 \
( \
    `EventTime` DateTime, \
    `EventDate` Date, \
    `IP` String, \
    `Uid` UInt32, \
    `Cid` String, \
    `Ctitle` String, \
    `Vid` String, \
    `Vtitle` String, \
    `Source` String, \
    `Logtype` String, \
    `City` String, \
    `Duration` UInt16, \
    `Pageid` UInt8 \
) \
ENGINE = MergeTree() \
PARTITION BY toYYYYMM(EventDate) \
ORDER BY (EventDate, intHash32(Uid)) \
SAMPLE BY intHash32(Uid);

select EventTime,Uid,Vid,Source,Logtype,City,Duration from SNM_initial20210916 limit 10; 
select count(0) from SNM_initial20210916; 

 SELECT
    toDate(EventTime) AS d,
    Source,
    Uid,
    Vid,
    min(EventTime) AS from,
    max(EventTime) AS to,
    count() AS heart_count,
    sum(Duration) AS total_duration
FROM SNM_initial20210916
WHERE (d = toDate(Now())) AND (Uid != 0) AND (Vid != '0')
GROUP BY
    Source,
    Uid,
    Vid,
    d
ORDER BY heart_count DESC
LIMIT 10;

CREATE TABLE SNM_merger (
  d DateTime,
  Source String,
  Uid UInt32,
  Vid String,
  From DateTime,
  To DateTime,
  Heart_count UInt32,
  Total_duration UInt32
)
 ENGINE = SummingMergeTree() 
 PARTITION BY toYYYYMM(d) 
 ORDER BY (Source, Uid) ;

SELECT  groupBitmap(toUInt64(Uid)) as onlineNum from SNM_initial20210916 where toDate(EventTime) = '2021-09-18';


SELECT  bitmapToArray(groupBitmapState(toUInt64(Uid))) from SNM_initial20210916 where toDate(EventTime) = '2021-09-18';

select bitmapCardinality(groupBitmapState(toUInt64(Uid))) from SNM_initial20210916 where toDate(EventTime) = '2021-09-18';

SELECT count(uid) from (
SELECT distinct Uid as uid from SNM_initial20210916 where toDate(EventTime) = '2021-09-18');


CREATE TABLE SNM_online_bitmap (
  d DateTime comment '日期',
  uv AggregateFunction(groupBitmap,UInt64) comment 'bitmap存储去重的上线用户'
)
 engine=AggregatingMergeTree()
partition by d
order by (d);

INSERT into SNM_online_bitmap 
	SELECT 
		toDate(EventTime) d,
		groupBitmapState(toUInt64(Uid)) uv
	FROM SNM_initial20210916 
	where toDate(EventTime) = '2021-09-18' or toDate(EventTime) = '2021-09-19'
	group by toDate(EventTime);

	SELECT 
		toDate(EventTime) d,
		groupBitmap(toUInt64(Uid)) uv
	FROM SNM_initial20210916 
	where toDate(EventTime) = '2021-09-18' or toDate(EventTime) = '2021-09-19'
	group by toDate(EventTime);
with(
SELECT uv from SNM_online_bitmap where toDate(d) = '2021-09-18'
) as a0918,
(
SELECT uv from SNM_online_bitmap where toDate(d) = '2021-09-19'
) as a0919
select bitmapAndCardinality(a0918,a0919) as UserKeepCounts;


CREATE TABLE SNM_Agg_User_D (
  datekey String,
  Source String,
  pt String,
  active_count UInt32,
  active_sum UInt32,
  active_rate UInt32,
  total_play UInt32,
  play_mans UInt32,
  play_count UInt32
)
 ENGINE = SummingMergeTree() 
 PARTITION BY Source  
 ORDER BY (datekey) ;

select * from SNM_Agg_User_D

CREATE View v_active_count as
 SELECT
    d,
    Source,
    count(Uid) as actives,
    9999 as column1
FROM SNM_merger
where Total_duration > 300
GROUP BY
    Source,d
ORDER BY
    actives desc 
limit 4;

SELECT d,Source,actives,column1 from v_active_count;

CREATE View v_total_duration as
 SELECT
    d,
    Source,
    sum(Total_duration) as total_duration
FROM SNM_merger
GROUP BY
    Source,d
ORDER BY
    total_duration desc ;

SELECT * from v_total_duration;

 SELECT
    d,
    Source,
    sum(Total_duration) as total_duration
FROM SNM_merger
GROUP BY
    Source,d
ORDER BY
    total_duration desc 
limit 4;


 SELECT
    d,
    Source,
    count(Uid)
FROM SNM_merger
WHERE (d = toDate(Now()))
GROUP BY
    Source,
    Uid;


CREATE MATERIALIZED VIEW mview_merge 
TO SNM_merger
AS 
 SELECT
    toDate(EventTime) AS d,
    Source,
    Uid,
    Vid,
    min(EventTime) AS From,
    max(EventTime) AS To,
    count() AS Heart_count,
    sum(Duration) AS Total_duration
FROM SNM_initial20210916
WHERE (d = toDate(Now())) AND (Uid != 0) AND (Vid != '0')
GROUP BY
    Source,
    Uid,
    Vid,
    d;

insert into SNM_merger 
SELECT
    toDate(EventTime) AS d,
    Source,
    Uid,
    Vid,
    min(EventTime) AS From,
    max(EventTime) AS To,
    count() AS Heart_count,
    sum(Duration) AS Total_duration
FROM SNM_initial20210916
WHERE (d = toDate(yesterday())) AND (Uid != 0) AND (Vid != '0')
GROUP BY
    Source,
    Uid,
    Vid,
    d;


  select  toDate(Now()-1);


select * from mview_merge;
select toDate(d),Uid,Vid,Source,From,To,Heart_count,Total_duration from SNM_merger order by d desc;

select count() from SNM_initial20210916;
select count() from SNM_merger;

 SELECT  count(0) 
 FROM SNM_initial20210916 
 Where toDate(EventTime) = toDate(Now()) and Uid=6 and Vid='2' and Source='mtv' 



SELECT
    table AS `表名`,
    sum(rows) AS `总行数`,
    formatReadableSize(sum(data_uncompressed_bytes)) AS `原始大小`,
    formatReadableSize(sum(data_compressed_bytes)) AS `压缩大小`,
    round((sum(data_compressed_bytes) / sum(data_uncompressed_bytes)) * 100, 0) AS `压缩率`
FROM system.parts
GROUP BY table;

select
    sum(rows) as row,--总行数
    formatReadableSize(sum(data_uncompressed_bytes)) as ysq,--原始大小
    formatReadableSize(sum(data_compressed_bytes)) as ysh,--压缩大小
    round(sum(data_compressed_bytes) / sum(data_uncompressed_bytes) * 100, 0) ys_rate--压缩率
from system.parts



select
    database,
    table,
    formatReadableSize(size) as size,
    --formatReadableSize(bytes_on_disk) as bytes_on_disk,
    --formatReadableSize(data_uncompressed_bytes) as data_uncompressed_bytes,
    --formatReadableSize(data_compressed_bytes) as data_compressed_bytes,
    compress_rate,
    rows,
    days,
    formatReadableSize(avgDaySize) as avgDaySize
from
(
    select
        database,
        table,
        sum(bytes) as size,
        sum(rows) as rows,
        min(min_date) as min_date,
        max(max_date) as max_date,
        sum(bytes_on_disk) as bytes_on_disk,
        sum(data_uncompressed_bytes) as data_uncompressed_bytes,
        sum(data_compressed_bytes) as data_compressed_bytes,
        (data_compressed_bytes / data_uncompressed_bytes) * 100 as compress_rate,
        max_date - min_date as days,
        size / (max_date - min_date) as avgDaySize
    from system.parts
    where active 
    group by
        database,
        table
);