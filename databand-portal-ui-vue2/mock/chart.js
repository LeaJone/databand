
module.exports = [
  {
    url: '/chart/dataset/datasetbytype',
    type: 'get',
    response: config => {
      const { datetype, producttype, channel, from, to } = config.query
      if (producttype === '2' || channel === 'b') {
        return {
          code: 200,
          data: {
            source: [
              ['product', 'Matcha1', 'Milk1', 'Cheese1'],
              ['2017', 43.3, 85.8, 93.7],
              ['2018', 83.1, 73.4, 55.1],
              ['2019', 86.4, 65.2, 82.5],
              ['2020', 96.4, 35.2, 52.5]
            ]
          }
        }
      }
      if (from !== '' || to !== '') {
        return {
          code: 200,
          data: {
            source: [
              ['product', 'Matcha2', 'Milk2', 'Cheese2'],
              ['2017', 43.3, 85.8, 93.7],
              ['2018', 83.1, 73.4, 55.1],
              ['2019', 86.4, 65.2, 82.5],
              ['2020', 96.4, 35.2, 52.5]
            ]
          }
        }
      }
      if (datetype === 'y') {
        return {
          code: 200,
          data: {
            source: [
              ['product', 'Matcha', 'Milk', 'Cheese'],
              ['2017', 43.3, 85.8, 93.7],
              ['2018', 83.1, 73.4, 55.1],
              ['2019', 86.4, 65.2, 82.5],
              ['2020', 96.4, 35.2, 52.5]
            ]
          }
        }
      }
      if (datetype === 'm') {
        return {
          code: 200,
          data: {
            source: [
              ['product', 'Matcha', 'Milk', 'Cheese'],
              ['2017-1', 23.3, 85.8, 93.7],
              ['2018-1', 83.1, 33.4, 55.1],
              ['2019-1', 86.4, 45.2, 82.5],
              ['2020-1', 96.4, 35.2, 62.5]
            ]
          }
        }
      }
      if (datetype === 'd') {
        return {
          code: 200,
          data: {
            source: [
              ['product', 'Matcha', 'Milk', 'Cheese'],
              ['2021-1-1', 43.3, 85.8, 63.7],
              ['2021-1-2', 83.1, 83.4, 55.1],
              ['2021-1-3', 76.4, 65.2, 82.5],
              ['2021-1-4', 96.4, 95.2, 52.5]
            ]
          }
        }
      }
      if (datetype === 'q') {
        return {
          code: 200,
          data: {
            source: [
              ['product', 'Matcha', 'Milk', 'Cheese'],
              ['2017Q1', 43.3, 45.8, 93.7],
              ['2018Q2', 33.1, 73.4, 55.1],
              ['2019Q3', 86.4, 55.2, 82.5],
              ['2020Q4', 56.4, 35.2, 52.5]
            ]
          }
        }
      }
      if (datetype === 'w') {
        return {
          code: 200,
          data: {
            source: [
              ['product', 'Matcha', 'Milk', 'Cheese'],
              ['2017-1W1', 43.3, 85.8, 53.7],
              ['2018-1W2', 83.1, 43.4, 55.1],
              ['2019-1W3', 36.4, 65.2, 82.5],
              ['2020-1W4', 96.4, 45.2, 22.5]
            ]
          }
        }
      }
    }
  },
  {
    url: '/chart/dataset/series',
    type: 'get',
    response: config => {
      return {
        code: 200,
        data: [
          { type: 'bar' },
          { type: 'bar' },
          { type: 'bar' }
        ]
      }
    }
  },
  {
    url: '/chart/radar/data',
    type: 'get',
    response: config => {
      return {
        code: 200,
        data: { legend: ['预算分配（Allocated Budget）', '实际开销（Actual Spending）'],
          indicator: [
            { name: '销售（sales）', max: 6500 },
            { name: '管理（Administration）', max: 16000 },
            { name: '信息技术（Information Techology）', max: 30000 },
            { name: '客服（Customer Support）', max: 38000 },
            { name: '研发（Development）', max: 52000 },
            { name: '市场（Marketing）', max: 25000 }
          ],
          series: [{
            name: '预算 vs 开销（Budget vs spending）',
            type: 'radar',
            // areaStyle: {normal: {}},
            data: [
              {
                value: [4300, 10000, 28000, 35000, 50000, 19000],
                name: '预算分配（Allocated Budget）'
              },
              {
                value: [5000, 14000, 28000, 31000, 42000, 21000],
                name: '实际开销（Actual Spending）'
              }
            ]
          }] }
      }
    }
  },
  {
    url: '/chart/pie/series',
    type: 'get',
    response: config => {
      const { datetype } = config.query
      if (datetype === 'y') {
        return {
          code: 200,
          data: [
            {
              name: '访问来源',
              type: 'pie',
              radius: '50%',
              data: [
                { value: 390, name: '搜索引擎' },
                { value: 735, name: '直接访问' },
                { value: 580, name: '邮件营销' },
                { value: 450, name: '联盟广告' },
                { value: 300, name: '视频广告' }
              ],
              emphasis: {
                itemStyle: {
                  shadowBlur: 10,
                  shadowOffsetX: 0,
                  shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
              }
            }
          ]
        }
      }
      if (datetype === 'q') {
        return {
          code: 200,
          data: [
            {
              name: '访问来源',
              type: 'pie',
              radius: '50%',
              data: [
                { value: 290, name: '搜索引擎' },
                { value: 635, name: '直接访问' },
                { value: 480, name: '邮件营销' },
                { value: 350, name: '联盟广告' },
                { value: 100, name: '视频广告' }
              ],
              emphasis: {
                itemStyle: {
                  shadowBlur: 10,
                  shadowOffsetX: 0,
                  shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
              }
            }
          ]
        }
      }
      if (datetype === 'm') {
        return {
          code: 200,
          data: [
            {
              name: '访问来源',
              type: 'pie',
              radius: '50%',
              data: [
                { value: 590, name: '搜索引擎' },
                { value: 635, name: '直接访问' },
                { value: 380, name: '邮件营销' },
                { value: 650, name: '联盟广告' },
                { value: 400, name: '视频广告' }
              ],
              emphasis: {
                itemStyle: {
                  shadowBlur: 10,
                  shadowOffsetX: 0,
                  shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
              }
            }
          ]
        }
      } else {
        return {
          code: 200,
          data: [
            {
              name: '访问来源',
              type: 'pie',
              radius: '50%',
              data: [
                { value: 690, name: '搜索引擎' },
                { value: 335, name: '直接访问' },
                { value: 480, name: '邮件营销' },
                { value: 150, name: '联盟广告' },
                { value: 200, name: '视频广告' }
              ],
              emphasis: {
                itemStyle: {
                  shadowBlur: 10,
                  shadowOffsetX: 0,
                  shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
              }
            }
          ]
        }
      }
    }
  }

]

