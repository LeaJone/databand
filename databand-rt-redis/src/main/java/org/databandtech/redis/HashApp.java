package org.databandtech.redis;

import java.util.UUID;

import org.redisson.Redisson;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

public class HashApp {

	static int HashMAXCOUNT = 1000000;
	
	public static void main(String[] args) {
		redisHashmaptest();
	}

	private static void redisHashmaptest() {

		System.out.println("测试 redis的hashmap存储量和匹配查询速度");
		Config config = new Config();
		config.useSingleServer().setAddress("redis://192.168.13.118:6379");
		RedissonClient redisson = Redisson.create(config);
		RMap<String, String> map = redisson.getMap("map1");

		String key1 = "";
		for (int i = 0; i < HashMAXCOUNT; i++) {
			String uuid = UUID.randomUUID().toString().replaceAll("-", "");
			if (i == 100000) {
				key1 = uuid;
			}
			map.put(uuid, "1");
		}

		System.out.println("创建hashmap项目结束");

		System.out.println("map总计：" + map.size());
		long starttime = System.currentTimeMillis();
		String value1 = map.get(key1);
		long endtime = System.currentTimeMillis();
		long duration = endtime - starttime;
		System.out.println("value1=" + value1 + "单项的匹配时长： " + duration);
	}

}
