package org.databandtech.streamjob.source;

import java.io.Serializable;

public interface RichSourceFunction<OUT> extends Serializable {
	
	void open() throws Exception;
	void close() throws Exception;
	void run(OUT value) throws Exception;
	void cancel();

}
