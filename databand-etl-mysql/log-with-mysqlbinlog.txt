log-with-mysqlbinlog

MySQL的二进制日志binlog可以说是MySQL最重要的日志，记录了create、alter、drop、update,insert,delete
从binlog进行ETL还是数据同步都是非常好的方式，非常灵活方便

判断MySQL是否已经开启binlog?
登录mysql，执行：SHOW VARIABLES LIKE 'log_bin';

OFF：关闭
ON：开启

找到my.conf的位置，使用命令
mysql --help | grep 'Default options' -A 1
/etc/my.cnf /etc/mysql/my.cnf /usr/etc/my.cnf ~/.my.cnf

vi /etc/my.cnf

添加开启binlog的配置，

log-bin=/var/lib/mysql/mysql-bin
#配置serverid
server-id=1

重启mysql
service mysqld restart
执行：
SHOW VARIABLES LIKE 'log_bin';
变on了就是开启binlog了

MySQL binlog的三种工作模式
（1）Row level
　　日志中会记录每一行数据被修改的情况，然后在slave端对相同的数据进行修改。
　　优点：能清楚的记录每一行数据修改的细节
　　缺点：数据量太大
（2）Statement level
　　每一条被修改数据的sql都会记录到master的bin-log中，slave在复制的时候sql进程会解析成和原来master端执行过的相同的sql再次执行
　　优点：解决了 Row level下的缺点，不需要记录每一行的数据变化，减少bin-log日志量，节约磁盘IO，提高新能
　　缺点：容易出现主从复制不一致
（3）Mixed（混合模式）
　　结合了Row level和Statement level的优点

查询binlog细节
show global variables like "binlog%";

+--------------------------------------------+--------------+
| Variable_name                              | Value        |
+--------------------------------------------+--------------+
| binlog_cache_size                          | 32768        |
| binlog_checksum                            | CRC32        |
| binlog_direct_non_transactional_updates    | OFF          |
| binlog_error_action                        | ABORT_SERVER |
| binlog_format                              | ROW          |
| binlog_group_commit_sync_delay             | 0            |
| binlog_group_commit_sync_no_delay_count    | 0            |
| binlog_gtid_simple_recovery                | ON           |
| binlog_max_flush_queue_time                | 0            |
| binlog_order_commits                       | ON           |
| binlog_row_image                           | FULL         |
| binlog_rows_query_log_events               | OFF          |
| binlog_stmt_cache_size                     | 32768        |
| binlog_transaction_dependency_history_size | 25000        |
| binlog_transaction_dependency_tracking     | COMMIT_ORDER |
+--------------------------------------------+--------------+

#查看日志开启状态 
show variables like 'log_%';
#查看所有binlog日志列表
show master logs;
#查看最新一个binlog日志的编号名称，及其最后一个操作事件结束点 
show master status;
#刷新log日志，立刻产生一个新编号的binlog日志文件，跟重启一个效果 
flush logs;
#清空所有binlog日志 
reset master;

修改binlog配置
[mysqld]
#设置日志三种格式：STATEMENT、ROW、MIXED 。
binlog_format = mixed
#设置日志路径，注意路经需要mysql用户有权限写
log-bin = /data/mysql/logs/mysql-bin.log
#设置binlog清理时间
expire_logs_days = 7
#binlog每个日志文件大小
max_binlog_size = 100m
#binlog缓存大小
binlog_cache_size = 4m
#最大binlog缓存大小
max_binlog_cache_size = 512m


在ROW模式下，binlog日志中的begin和commit之间并没有sql语句。
在STATEMENT模式下，binlog日志中的begin和commit之间是一条sql语句。

查看binlog文件不可使用vi或cat，使用：
mysqlbinlog /var/lib/mysql/mysql-bin.000001


从binlog日志恢复数据

a、恢复命令的语法格式：

　　　mysqlbinlog mysql-bin.0000xx | mysql -u用户名 -p密码 数据库名

b、常用参数选项解释：

　　　--start-position=875 起始pos点
　　　--stop-position=954 结束pos点
　　　--start-datetime="2016-9-25 22:01:08" 起始时间点
　　　--stop-datetime="2019-9-25 22:09:46" 结束时间点
　　　--database=ops指定只恢复ops数据库(一台主机上往往有多个数据库，只限本地log日志)

c、不常用选项：

　　　-u --user=name 连接到远程主机的用户名
　　　-p --password[=name]连接到远程主机的密码
　　　-h --host=name 从远程主机上获取binlog日志
　　　--read-from-remote-server从某个Mysql服务器上读取binlog日志
